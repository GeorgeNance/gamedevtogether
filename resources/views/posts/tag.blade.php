@extends('layouts.app')

@section('content')

@section("title","Game Dev Together")
<div class="max-w-2xl mx-auto h-screen mt-8">
    <div class="flex w-full justify-center ">
        <div class="hidden lg:flex flex-col lg:w-1/4">
            <div class="panel text-center p-4">
                <h2 class="w-full border-b-2 border-grey-dark mb-2">#{{$tag->name}}</h2>
                <div class="w-full">This is a tag filter page. More comming soon</div>
            </div>
            <div class="panel p-4 mt-4">

            </div>
        </div>
        <div class="flex flex-col sm:w-full md:w-1/2 mx-4">
            <post-cards :tag="'{{$tag->name}}'"></post-cards>
            {{--@each('posts.card', $posts, 'post')--}}
        </div>

    </div>
</div>
</div>
@endsection
