@extends('layouts.app')

@section('content')
    <div class="flex max-w-3xl mx-auto h-screen lg:mt-8 justify-center">
        <div class="flex flex-col xl:w-2/3 w-full max-w-xl xl:mr-4">

            <div class="panel m:p-4">

                <article>
                    @isset($post->image)
                    <section>
                            <div class="w-full image"
                                 style='background-image: url("{{$post->image}}"); background-color: #1f9d55; background-size: cover;'></div>
                    </section>
                    @endisset
                    <div class="">
                        <header class="mb-2 title p-2 sm:p-12">
                            <h1 class="text-5xl mb-4 font-medium">{{$post->title}}</h1>
                            <h3><a href="/{{$post->user->username}}">
                                    <img class="h-6 float-left profile-image" src="{{$post->user->profileImage()}}">
                                    <p class="text-left font-medium  mb-1 mx-2 float-left">{{$post->user->name}}</p></a>
                                <p class="text-grey font-light text-sm">{{$post->created_at->format("M d `Y")}}</p>
                                @auth
                                    @if ($post->user->id == $loggedInUser->id)
                                    <a href="{{route("posts.edit",[$post->user->username,$post->slug])}}"><button class="edit-button float-right p-1">Edit</button></a>
                                        @endif
                                @endauth
                            </h3>
                            <div class="mt-4 flex flex-row justify-start">
                                @foreach($post->tags as $tag)
                                    <div class="tag bg-grey"><a class="text-black" href="/t/{{$tag->name}}"> #{{$tag->name}} </a></div>
                                @endforeach
                            </div>


                        </header>
                        <div class="body" data-post-id="{{$post->id}}" id="post-body">
                            {!!$post->body()!!}
                        </div>
                    </div>
                </article>
                <reaction-bar :source_id="{!! $post->id !!}"></reaction-bar>
                <br>
                <br>
                <div class="p-4 md:p-12">
                    <div class="w-full">
                        <div class="px-4 pt-4 pb-10 bg-grey-lightest border border-grey rounded mb-8">
                            @auth
                                <form method="post" action="{{route("comments.store")}}">
                                    <input type="hidden" name="comment[user_id]" value="{{$loggedInUser->id}}">
                                    <input type="hidden" name="comment[post_id]" value="{{$post->id}}">
                                    @csrf
                                    <textarea class=" p-2 w-full h-24 rounded border border-light mb-2 resize-none"
                                              placeholder="Add to the discussion"
                                              name="comment[body_markdown]"></textarea>
                                    <button type="submit"
                                            class="float-right bg-blue text-white  border p-2 rounded">
                                        Submit
                                    </button>
                                </form>
                            @endauth
                            @guest
                                <p class="title"><a href="/login">Sign in</a> to Comment</p>
                            @endguest
                        </div>


                        @each('posts.comment', $post->comments, 'comment')
                    </div>
                </div>
            </div>
        </div>
        <div class="xl:flex hidden xl:flex-col xl:w-1/4">
            <div class="panel">
                <div class="p-4 mb-8">
            <a class="" href="/{{$post->user->username}}">
                <div class="float-left h-full">
                        <img class="h-8  rounded-full" src="{{$post->user->profileImage()}}">
                </div>
                <div class="ml-8 pl-3">
                    <h3 class="text-left font-bold text-black">{{$post->user->name}}</h3>
                    {{--<div class="font-mono text-sm text-grey-dark mt-1">{{ "@".$post->user->username }}</div>--}}
                </div>
                <div class="mt-6">
                    <div class="italic text-grey-dark text-sm">{{ $post->user->bio }}</div>
                </div>
                <div class="mt-4">
                    <div class="font-mono text-grey-dark font-bold text-sm">location</div>
                    <div class="font-mono text-grey-darkest mt-1 font-bold">{{ $post->user->location }}</div>
                </div>
                <div class="mt-2">
                    <div class="font-mono text-grey-dark font-bold text-sm">joined</div>
                    <div class="font-mono text-grey-darkest mt-1 font-bold">{{$post->user->created_at->format("M d, Y")}}</div>
                </div>
            </a>
                    {{--<button class="block border bg-blue ">Follow</button>--}}

                </div>
            </div>
            <div class=" mt-8">
                <h4 class="w-full border-b-2 border-grey-dark mb-2 text-lg">More from {{"@".$post->user->username}}</h4>
                <div class="w-full">

                        @foreach($post->user->posts()->latest()->take(3)->get() as $post)
                        <div class="panel overflow-hidden mt-2">
                            <div class="p-4">
                                <img class="w-4 h-4 rounded-full float-left " src="{{$post->user->profileImage()}}" alt="Avatar of Jonathan Reinink">
                                <div class="text-left mb-2 ml-6">
                                    <a href="{{route("posts.show",[$post->user->username,$post->slug])}}">
                                        <p class="text-black font-semibold">{{$post->title}}</p>
                                    </a>
                                </div>


                            </div>
                            <a class="" href="{{route("posts.show",[$post->user->username,$post->slug])}}">
                            </a>
                        </div>
                        @endforeach

                </div>
            </div>

        </div>
    </div>
@endsection
