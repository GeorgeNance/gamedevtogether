<div class="mb-4">

<div class="panel overflow-hidden ">
        {{--@isset ($post->image)--}}
        {{--<a class="no-underline" href="{{route("posts.show",[$post->user->username,$post->slug])}}">--}}
    {{--<img class="w-full" src="{{$post->image}}" alt="Placeholder image">--}}
    {{--</a>--}}
        {{--@endisset--}}

        <div class="px-4 py-6">
            <img class="w-12 h-12 rounded-full mr-4 float-left " src="{{$post->user->profileImage()}}" alt="Avatar of Jonathan Reinink">
            <div class="px-12 text-left mb-2 ml-4">
                <a class="no-underline" href="{{route("posts.show",[$post->user->username,$post->slug])}}">
                <p class="text-2xl text-black font-semibold">{{$post->title}}</p>
                </a>
                <div class="text-sm py-1 float-left hover:underline">
                    <a class="" href="/{{$post->user->username}}"> <p class="text-black float-left inline-block">{{$post->user->name}}</p>
                    <p class="text-grey-dark float-left">&nbsp;◆&nbsp;{{$post->created_at->format("M d `y")}}</p></a>
                </div>
            </div>


        </div>
            <a class="" href="{{route("posts.show",[$post->user->username,$post->slug])}}">
            <div class="flex flex-row text-sm  px-4 justify-start mb-2">
                <div class="text-grey-dark text-left"><img class="h-4" src="{{ asset('images/heart.png') }}"/><span class="">{{$post->hearts_cache}}</span></div>


                    <p class="ml-2 text-grey-dark text-left hover:underline"><i class="fa fa-comments" aria-hidden="true"></i> {{$post->comments_cache}}</p>

            </div>
            </a>
</div>
    </a>
</div>
