@extends('layouts.app')
@section("title","Edit - Game Dev Together")
@section('content')
    <div class="flex items-center h-screen w-full mt-8">
        <div class="w-full panel md:max-w-xl md:mx-auto mt-8 p-4">
            <div class="mb-6 md:1/2 px-2">
                @if ($errors->any())
                    <div class="bg-red-lighter border border-red-dark">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @isset($post)
                <form class="" action="{{route("posts.update",[$post->user->username,$post->slug])}}" method="post">

                    <post-editor :post="{{json_encode($post)}}"></post-editor>
                    @else
                        <form class="" action="{{route("posts.store")}}" method="post">

                            <post-editor></post-editor>
                    @endisset

                            @csrf
                </form>

            </div>
        </div>
    </div>
@endsection