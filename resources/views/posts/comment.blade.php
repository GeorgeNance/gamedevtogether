<div class="">
<div class="border border-grey-light p-4">
    <div class="flex flex-row justify-between items-center border-b pb-1">
        <a class="" href="/{{$comment->user->username}}">
                        <span class="text-black align-middle">
            <img class="profile-image h-8 align-middle " src="{{$comment->user->profileImage()}}">

                <span class="ml-2 font-medium text-grey-dark">{{$comment->user->name}}</span>
            </span>
        </a>
        <div class="float-right">
            <p class="text-grey">{{$comment->created_at->format('M d `y')}}</p>
        </div>
    </div>
    <div class="body">
{!! $comment->body_html !!}
    </div>

</div>
</div>