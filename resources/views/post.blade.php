@extends('layouts.app')

@section('content')

    <h1>{{$post->title}}</h1>
    <br>
    <div class="row">
 {!!$post->body()!!}
    
    <hr>
    </div>




<div class="columns">
    <div class="column">
        @include('includes.sidebar')
    </div>
    <div class="column is-two-thirds">
        @each('includes.post', $posts, 'post')
    </div>
    <div class="column">
        Third column
    </div>
</div>
@endsection
