@extends('layouts.app')
@section("title","Sign In - Game Dev Together")
@section('content')
    @include("includes.svg")
    <div class="flex items-center w-full mt-8">
        <div class="w-full bg-white rounded shadow-lg p-8 m-4 h-64   md:max-w-xl md:mx-auto">
            <div class="flex flex-col p-12 mx-auto xl:w-1/2 text-center">
                <h1 class="float-left">Sign in</h1>
                <a class="no-underline" href="{{route("login.social","twitter")}}">
                <button class="bg-blue border-black p-4 rounded my-2 text-white">

                    <i class="fab  fa-twitter"></i> Sign in with Twitter
                    </button>
                </a>
                <p>We require social login to prevent abuse</p>
            </div>

        </div>
    </div>
@endsection
