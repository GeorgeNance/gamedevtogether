@extends('layouts.app')
@section("title","Settings - Game Dev Together")
@section('content')
    <div class="mt-8">
        <div class="flex items-center w-full">
            <div class="w-full panel p-8 md:m-4 md:max-w-lg md:mx-auto">
                @if ($errors->any())
                    <div class="bg-red-lighter border border-red-dark">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="mb-6 max-w-md mx-auto" action="{{route("users.update",[$user->username])}}" method="post">

@csrf

                    <div class="field-group mb-4">
                        <label class="field-label" for="name">Name</label>
                        <input class="field" type="text" name="user[name]" id="name" autocomplete="off"
                               value="{{$user->name}}" required>
                    </div>
                    <div class="field-group mb-4">
                        <label class="field-label" for="picture">Profile Picture</label>
                        <input class="field" type="text" name="user[profile_image]" id="picture" autocomplete="off"
                               value="{{$user->profile_image}}" required>
                    </div>
                    <div class="field-group mb-4">
                        <label class="field-label" for="username">Username</label>
                        <input class="field" type="text" name="user[username]" id="username" autocomplete="off"
                               value="{{$user->username}}" required>
                    </div>
                    <div class="field-group mb-4">
                        <label class="field-label" for="email">Email</label>
                        <input class="field" type="email" name="user[email]" id="username" autocomplete="off"
                               value="{{$user->email}}" required>
                    </div>
                    <div class="field-group mb-4">
                        <label class="field-label" for="website_url">Website Url</label>
                        <input placeholder="http://yoursite.com" class="field" type="text" name="user[website_url]" id="website_url" autocomplete="off"
                               value="{{$user->website_url}}" >
                    </div>
                    <div class="field-group mb-4">
                        <label class="field-label" for="bio">Bio</label>
                        <textarea maxlength="200" class="field" type="text" name="user[bio]" id="bio"
                                  autocomplete="off" placeholder="A short bio">{{$user->bio}}</textarea>
                    </div>
                    <div class="field-group mb-4">
                        <label class="field-label" for="bio">Background Color</label>
                        <input class="field jscolor{hash:true}" type="text" name="user[background_color]" id="bio" autocomplete="off"
                               value="{{$user->background_color}}" >
                    </div>

                    <h3 class="mb-4 uppercase">Links</h3>
                    <div class="field-group mb-4">
                        <label class="field-label" for="twitter">Twitter Username</label>
                        <input class="field" type="text" name="user[twitter_username]" id="twitter" autocomplete="off"
                               value="{{$user->twitter_username}}" >
                    </div>

                    <div class="field-group mb-4">
                        <label class="field-label" for="twitch">Twitch Username</label>
                        <input class="field" type="text" name="user[twitch_username]" id="twitch" autocomplete="off"
                               value="{{$user->twitch_username}}" >
                    </div>

                    <div class="field-group mb-4">
                        <label class="field-label" for="steam">Steam Username</label>
                        <input class="field" type="text" name="user[steam_username]" id="steam" autocomplete="off"
                               value="{{$user->steam_username}}" >
                    </div>
                    <div class="md:w-full float-right mt-8">
                        <button class="block bg-green hover:bg-green-dark float-right text-white uppercase text-lg mx-2 p-4 rounded"
                                type="submit">Save
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/jscolor.js') }}" defer></script>
@endsection
