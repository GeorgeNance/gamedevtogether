@extends('layouts.app')
@section("title","$user->name - Game Dev Together")
@section('content')
    @include("includes.svg")
    <div class="mt-8 h-screen">

        <div class="w-full  md:max-w-xl md:mx-auto">
            <div class="panel" style="border-color: {{$user->background_color}}">
            <div class="flex w-full p-10 ">
                <div class="flex flex-col">
                    <div class="float-left mr-4">
                        <img style="border-color: {{$user->background_color}}" class="w-32 profile-image border-4 block" src="{{$user->profile_image}}">
                    </div>
                </div>
                <div class="flex flex-col mx-4 w-1/2">
                    <h1 class="text-3xl mb-4">{{$user->name}}</h1>
                    @auth
                    @if($user->id == $loggedInUser->id)
                        <a href="{{route("users.edit")}}"><button style="background-color: {{$user->background_color}}" class="bg-blue border rounded p-2 w-32 text-white font-bold uppercase">Edit</button></a>
                    @endif
                    @endauth
                    <br>
                    <div class="text-grey-dark italic mt-2">
                        @isset($user->bio)
                        {{$user->bio}} @endisset
                    </div>
                    <div class="flex flex-row mt-4" >
                        @isset($user->twitter_username)
                            <div class="m-2">
                            <a  href="http://twitter.com/{{$user->twitter_username}}"><i class="fab fa-2x fa-twitter text-black" style="color: {{$user->background_color}}"></i></a>
                            </div>
                            @endisset
                            @isset($user->twitch_username)
                                <div class="m-2">
                                <a  href="http://twitch.tv/{{$user->twitch_username}}"><i class="fab fa-2x fa-twitch text-black" style="color: {{$user->background_color}}"></i></a>
                                </div>
                            @endisset
                            @isset($user->steam_username)
                                <div class="m-2">
                                    <a  href="https://steamcommunity.com/id/{{$user->steam_username}}"><i class="fab fa-2x fa-steam text-black" style="color: {{$user->background_color}}"></i></a>
                                </div>
                            @endisset
                    </div>
                </div>
                <div class="hidden md:flex flex-col mx-4 border-l-2 p-1">
                    <div class="ml-2">
                        <div class="mt-2">
                            <div class="font-mono text-grey-dark mb-1">location</div>
                            <div class="font-medium text-lg">{{$user->location}}</div>
                        </div>
                        <div class="mt-4">
                            <div class="font-mono text-grey-dark mb-1">joined</div>
                            <div class="font-medium text-lg">{{$user->created_at->format("M d, Y")}}</div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="flex justify-center mt-12">
            <div class="flex flex-col w-full md:w-3/5">
                @each('posts.card', $user->posts, 'post')
            </div>

            </div>
        </div>

    </div>
@endsection
