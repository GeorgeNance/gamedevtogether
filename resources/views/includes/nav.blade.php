<nav class="w-full fixed  text-left py-1 sm:px-20 px-8 bg-grey-darkest shadow-md border-b-4 border-teal">
    <div class="flex justify-between flex-row py-1  mx-auto items-center">
        <div class="flex flex-col">
            <a href="/" class="text-2xl no-underline text-white">Home</a>
        </div>

        <div>
            <a href="/new" class="no-underline ">
                <button class="bg-orange hover:bg-orange-dark border-2 border-orange-dark text-black uppercase mx-auto p-1 rounded">
                    Create A Post
                </button>
            </a>

                <navbar-account></navbar-account>
        </div>
    </div>

</nav>