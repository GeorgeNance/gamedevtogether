
    <div class="panel hover:bg-blue-lightest">
        <div class="px-2 py-4 mx-auto flex items-center">
            @auth
                <a href="{{route("users.profile",$loggedInUser->username)}}">
            <img class="profile-image inline h-12 float-left mb-2"
                 src="{{$loggedInUser->profileImage()}}"
                 alt="">
            <div class="flex flex-col px-2">
                <p class="text-left mb-1 text-md font-sans text-black">{{$loggedInUser->name}}</p>
                <p class="text-grey text-sm font-mono">{{"@".$loggedInUser->username}}</p>
            </div>
                </a>
            @endauth
            @guest
                    <a class="no-underline mx-auto" href="/login"><button class="  text-white font-bold bg-purple border-black p-2 rounded-lg text-center">Sign in for awesome</button></a>
                @endguest
        </div>

    </div>
    @auth
    <div class="panel mt-4">
        <div class="px-2 py-4">

                <div class="flex flex-col px-2">
                    <div class="px-4">
                        @foreach(\App\Tag::all() as $tag)
                    <a href="{{$tag->slug()}}"> <p class="text-black text-left mb-1 text-md font-sans">#{{$tag->name}}</p></a>
                            @endforeach
                    </div>
                </div>

        </div>

    </div>

    @endauth