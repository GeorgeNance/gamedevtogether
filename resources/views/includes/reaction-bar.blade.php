<div class="w-full">
    <div class="flex flex-row bg-blue-lightest border-blue-light px-12 py-6 h-20 border-t-2 border-b-2 text-center">
        <button data-liked="false" data-type="heart" onclick="reactButtonClick(this)" class="reactButton border-2 border-grey-light rounded-full w-1/4 hover:bg-blue-lighter"><img class="align-middle mr-2" src="{{ asset('images/heart.png') }}"/><span class="inline-block text-xl font-bold h-6 ml-2 align-middle" id="react-number-heart">{{$post->reactions->where("type","heart")->count()}}</span>
        </button>
    </div>
</div>
