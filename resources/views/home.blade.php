@extends('layouts.app')

@section('content')

@section("title","Game Dev Together")
<div class="max-w-3xl mx-auto h-screen mt-8">
    <div class="flex w-full justify-center ">
        <div class="md:flex flex-col hidden max-w-sm md:w-1/4 xl:w-1/4">
            @include('includes.sidebar')
        </div>
        <div class="flex flex-col sm:w-full md:w-1/2 mx-4">
            <post-cards></post-cards>
            {{--@each('posts.card', $posts, 'post')--}}
        </div>
        <div class="hidden lg:flex flex-col lg:w-1/4">
            <div class="panel text-center p-4">
                <h2 class="w-full border-b-2 border-grey-dark mb-2">Welcome! 👋</h2>
                <div class="w-full">This is a community website for game developers, artists, and anyone interested in video game creation. Please don't stare at the bugs directly, the site is still in beta. 👾</div>
            </div>
            <div class="panel p-4 mt-4">
                <h2 class="w-full border-b-2 border-grey-dark mb-2 text-center">Recent Posts</h2>
                <div class="w-full">
                    <ul>
                        @foreach($recentPosts as $post)
                            <li><a href="{{$post->url()}}" class="text-sm"><span class="text-grey-darker">{{$post->title}}</span> - <span class="text-grey-dark">{{$post->user->name}}</span></a></li>
                        @endforeach
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
