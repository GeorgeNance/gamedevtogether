<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/stylesheet.css') }}" rel="stylesheet">
</head>
<body class="bg-grey-lighter">
<div id="app">
    @include("includes.nav")
    <div class="py-12">

        @yield('content')
    </div>
</div>
<!-- Scripts -->
<script>
    @auth
    let user = {!! Auth::user() ? : '[]' !!};
    @endauth
</script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
