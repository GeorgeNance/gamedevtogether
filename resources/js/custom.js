window.reactButtonClick = function(button){
    console.log()
    var xhr = new XMLHttpRequest();
    // Create and send a GET request
// The first argument is the post type (GET, POST, PUT, DELETE, etc.)
// The second argument is the endpoint URL
    xhr.open('POST', '/reactions',true);
    var post = document.getElementById('post-body').dataset.postId;
    console.log(post);
    xhr.setRequestHeader('X-CSRF-TOKEN', document.querySelector("meta[name='csrf-token']").getAttribute("content"));
    var type = button.dataset.type;
    var data = {"source":"post","source_id":post,"type":type}
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(data));

    // Setup our listener to process completed requests
    xhr.onload = function () {

        // Process our return data
        if (xhr.status >= 200 && xhr.status < 300) {
            // This will run when the request is successful
            let result = JSON.parse(xhr.responseText);
            if (result.result == "create") {
                document.getElementById('react-number-' + type).innerHTML = parseInt(document.getElementById('react-number-' + type).innerText) + 1;
            }else{
                document.getElementById('react-number-' + type).innerHTML = parseInt(document.getElementById('react-number-' + type).innerText) - 1;

            }
        } else {
            // This will run when it's not
            console.log('The request failed!');
        }

        // This will run either way
        // All three of these are optional, depending on what you're trying to do
        console.log('This always runs...');
    };
}