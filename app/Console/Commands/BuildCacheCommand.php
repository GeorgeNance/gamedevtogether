<?php

namespace App\Console\Commands;

use App\Post;
use Illuminate\Console\Command;

class BuildCacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:build';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        foreach (Post::all() as $post){
            $post->comments_cache = $post->comments()->count();
            $post->hearts_cache = $post->reactions()->where("type","heart")->count();
            $post->save();
        }
    }
}
