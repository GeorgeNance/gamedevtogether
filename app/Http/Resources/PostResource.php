<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'user' => [
                "name"=>$this->user->name,
                "username"=>$this->user->username,
                "url"=>"/".$this->user->username,
                "profile_image"=>$this->user->profileImage()
            ],
            'tags'=>$this->tags->pluck("name"),
            'date' => $this->created_at,
            'body_markdown' => $this->body_markdown,
            'body_html' => $this->body_html,
            'hearts'=>$this->hearts_cache,
            'comments'=>$this->comments_cache,
            'url'=>"/".$this->user->username."/".$this->slug
        ];
    }
}
