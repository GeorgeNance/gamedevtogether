<?php

namespace App\Http\Middleware;

use Closure;

class AbortIfNotOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $resourceName)
    {
        if ($resourceName == "posts") {
            $post = $request->route()->post;
            $user = $request->user();
            if (!$user->isSuperUser() && $user->id != $post->user_id) {
                abort(403, 'Unauthorized action.');
            }
        }


        return $next($request);
    }
}
