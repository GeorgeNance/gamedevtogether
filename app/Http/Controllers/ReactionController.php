<?php

namespace App\Http\Controllers;

use App\Post;
use App\Reaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReactionController extends Controller
{
    //
    private $allowedReactions = ["heart"];


    public function react(Request $request)
    {

        if (!in_array($request->type, $this->allowedReactions)) {
            abort(422);
        }

        $user = Auth::user();
        $where = array(
            "source" => $request->source,
            "source_id" => $request->source_id,
            "type" => $request->type,
            "user_id" => $user->id
        );
        // Check if there is already a reaction
        //if so, remove it
        $reaction = Reaction::firstOrNew($where);
        if ($reaction->exists) {
            $reaction->active = !$reaction->active;
        }
        $reaction->save();
        return response()->json(array("result" => (!isset($reaction->active) || $reaction->active) ? "create" : "remove", "type" => $request->type));
    }

    public function show(Request $request)
    {

        $where = array(
            "source" => $request->source,
            "source_id" => $request->source_id,
            "active" => 1
        );

        $data["post_reactions_count"] = Reaction::select(DB::raw('type,count(*) as count'))->where($where)->groupBy("type")->get();

        if (Auth::check()) {
            $data["user"] = Reaction::where($where)->where("user_id", Auth::id())->get();
        }

        return response()->json($data);

    }
}
