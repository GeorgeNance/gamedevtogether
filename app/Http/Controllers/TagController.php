<?php

namespace App\Http\Controllers;

use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function show(Request $request,Tag $tag)
    {
        //
        $data["tag"] = $tag;
        //$data["posts"] = Post::latest()->with("user", "comments", "reactions")->orderBy("hearts_cache")->paginate(25);

        return view("posts.tag",$data);
    }

}
