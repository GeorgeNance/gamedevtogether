<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\LinkedSocialAccount;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        try {
            $user = \Socialite::with($provider)->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }

        $account = LinkedSocialAccount::where('provider_name', $provider)
            ->where('provider_id', $user->getId())
            ->first();

        if ($account) {
            $authUser = $account->user;
        } else {
            $authUser = null;
            if (isset($user->email)) {
                $authUser = User::where('email', $user->getEmail())->first();
            }
            if (!$authUser) {

                $authUser = new User([
                    'email' => $user->getEmail(),
                    'name' => $user->getName(),
                ]);

                if ($provider == "twitter") {
                    $authUser->username = $user->nickname;
                    $authUser->twitter_username = $user->nickname;
                    $authUser->profile_image = $user->avatar_original;
                }
                $authUser->save();
            }

            $authUser->accounts()->create([
                'provider_id' => $user->getId(),
                'provider_name' => $provider,
                'auth_data_dump' => json_encode($user)
            ]);


        }
        auth()->login($authUser, true);


        return redirect()->to($this->redirectTo);
    }


    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/');
    }
}
