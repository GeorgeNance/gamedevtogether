<?php

namespace App\Http\Controllers;

use App\Helpers\TagHelper;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Post;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Parsedown;

class PostController extends Controller
{
    private $parsedown;

    public function __construct()
    {
        $this->parsedown = new Parsedown();
        $this->parsedown->setSafeMode(true);
        $this->parsedown->setBreaksEnabled(true); #(true);
        $this->middleware('owner:posts', ['only' => ['edit', 'update']]);
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $tag = $request->tag;
        $filter = $request->filter;

        $posts = Post::with("user", "tags", "reactions");

        if (isset($tag)){
            $posts = $posts->whereHas("tags",function($q) use ($tag) {
                $q->where('name', '=', $tag);
            });
        }

        if(isset($filter)){
            $date = Carbon::now();
           switch ($filter){
               case "day":
                  $date =  $date->subDay(1);
                   $posts = $posts->where("created_at",">=",$date);
                  break;
               case "week":
                   $date =$date->subWeek(1);
                   $posts = $posts->where("created_at",">=",$date);
                   break;
               case "month":
                   $date =$date->subMonth(1);
                   $posts = $posts->where("created_at",">=",$date);
                   break;
               case "year":
                   $date = $date->subYear(1);
                   $posts = $posts->where("created_at",">=",$date);
                   break;
           }

           if ($filter=="recent"){
               $posts = $posts->latest();
           }else{
               $posts = $posts->orderBy("hearts_cache","desc");
           }


        }


        $posts = $posts
            ->orderBy("hearts_cache")->paginate(25);
        return new PostCollection($posts);
    }

    public function feed(Request $request)
    {

        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data["tags"] = Tag::take(100)->get();
        return view('posts.editor', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        //$post = $request->all()["post"];
        $input = $request->all();


//        $request->validate([
//            'post.title' => 'required|unique:posts,username,'.$user->id,
//            'post.markdown' => 'required',
//        ]);

        //
        $title = $input["post"]["title"];
        $body = $input["post"]["body"];
        /// PHP7+
        $numOfBytes = 4;
        $randomBytes = random_bytes($numOfBytes);
        $randomString = bin2hex($randomBytes);
        $clean_title = preg_replace('/[^\da-z ]/i', '', $title);
        $slug = urlencode(str_replace(' ', '-', strtolower($clean_title . $randomString)));


        $body_html = $this->parsedown->text($body);

        $tags = TagHelper::parseTag($input["post"]["tags"]);
        if (count($tags) > 4) {
            return redirect()->back()->withErrors(["Only 4 tags are allowed"]);
        }
//        $image = $request->post["image"];
        $post = Post::create([
            "title" => $title,
            "body_markdown" => $body,
            "body_html" => $body_html,
            "slug" => $slug,
            "user_id" => $user->id,
//            "image"=>$image
        ]);

        $post->syncTags($tags);


        return redirect()->route("posts.show", [$user->username, $slug]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Post $post)
    {
        //

        $data["post"] = $post;

        return view('posts.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, Post $post)
    {

        $data["post"] = new PostResource($post);


        return view('posts.editor', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, Post $post)
    {
        //
        //


        $title = $request->post["title"];
        $body = $request->post["body"];
        $numOfBytes = 4;
        $randomBytes = random_bytes($numOfBytes);
        $randomString = bin2hex($randomBytes);
        $clean_title = preg_replace('/[^\da-z ]/i', '', $title);
        $slug = urlencode(str_replace(' ', '-', strtolower($clean_title . $randomString)));

        $body_html = $this->parsedown->text($body);


        $tags = TagHelper::parseTag($request->post["tags"]);
        if (count($tags) > 4) {
            return redirect()->back()->withErrors(["Only 4 tags are allowed"]);
        }

        $post->update([
            "title" => $title,
            "body_markdown" => $body,
            "body_html" => $body_html,
            "slug" => $slug,
        ]);

        $post->syncTags($tags);

        return redirect()->route("posts.show", [$user->username, $slug]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
