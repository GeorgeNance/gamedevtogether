<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reaction extends Model
{
    //
    protected $fillable = ["user_id","source_id","source","type","created_at","active"];
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {

            $model->created_at = $model->freshTimestamp();

            if ($model->source=="post" && $model->type=="heart"){
                $post = Post::find($model->source_id);
                $post->hearts_cache++;
                $post->save();
            }
        });
    }
}
