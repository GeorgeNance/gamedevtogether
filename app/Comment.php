<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $with = ['user'];

    protected $fillable = ["post_id","user_id","body_markdown"];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $post = Post::find($model->post_id);
            $post->comments_cache++;
            $post->save();
        });
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
