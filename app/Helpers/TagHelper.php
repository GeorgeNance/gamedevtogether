<?php
/**
 * Created by PhpStorm.
 * User: George
 * Date: 1/4/19
 * Time: 7:53 PM
 */
namespace App\Helpers;

class TagHelper
{
    static function parseTag($tags)
    {
        return str_replace("#","",preg_split("/[\s, ]+/", $tags));
    }
}