<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $fillable = ["name"];

    public function getRouteKeyName()
    {
        return 'name';
    }

    public function slug()
    {
        return route("tag",$this->name);
    }
}
