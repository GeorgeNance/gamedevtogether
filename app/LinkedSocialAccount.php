<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkedSocialAccount extends Model
{
    //
    protected $fillable = ['provider_name', 'provider_id',"auth_data_dump" ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
