<?php

namespace App;

use App\Traits\HasTags;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Parsedown;

class Post extends Model
{
    use HasTags;

    //
    public $fillable = ["title","body_markdown","body_html","user_id","slug","hotness_score","image"];

    protected $with = ["user","tags"];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id', 'id');
    }

    public function reactions()
    {
        return $this->hasMany(Reaction::class, 'source_id', 'id')->where("source","post");
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'taggings', 'source_id',"tag_id")->where("source","post");
    }

    public function hotnessScore(){
        $points = $this->reactions()->where("type","heart")->count();
        $hours = Carbon::now()->diffInHours($this->created_at);
        $g = 1.2;
    return round(((($points+1)*10) / pow(($hours),$g))*1000);
    }

    public function updateHotnessScore(){
        $this->hotness_score = $this->hotnessScore();
        $this->save();
    }

    public function url(){

        return "/".$this->user->username."/".$this->slug;
    }



    public function body(){
        if (empty($this->body_html)){
            $parsedown = new Parsedown;
            $parsedown->setSafeMode(true);
            $this->update(["body_html"=>$parsedown->text($this->body_markdown)]);
        }
        return $this->body_html;
    }

    public function tagsString(){
        return implode(",",$this->tags()->pluck("name")->toArray());
    }




    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function sourceName()
    {
        return "post";
    }

}
