<?php namespace App\Traits;

use App\Tag;

/**
 * Created by PhpStorm.
 * User: George
 * Date: 1/4/19
 * Time: 7:42 PM
 */

trait HasTags
{
    public function syncTags($tagNames)
    {
        $tags = array();
        foreach ($tagNames as $tagName){
            $tag = Tag::firstOrCreate(["name"=>$tagName]);

            $tags[$tag->id] = array( 'source' => $this->sourceName() );
        }
        $this->tags()->sync($tags);

    }

    public function sourceName(){
        return "unknown";
    }

}
