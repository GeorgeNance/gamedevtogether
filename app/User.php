<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',"username","twitter_username","profile_image","twitch_username","steam_username","bio","background_color"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function profileImage(){
        if (isset($this->profile_image)){
            return $this->profile_image;
        }else{
            return "https://www.gravatar.com/avatar/".md5($this->email)."?s=64&d=retro";
        }

    }

    public function accounts(){
        return $this->hasMany('App\LinkedSocialAccount');
    }

    public function isSuperUser(){
        return $this->id == 1;
    }

    public static function forUsername($username){
        return User::where("username","=",$username)->first();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'user_id', 'id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'user_id', 'id');
    }

    public function getRouteKeyName()
    {
        return 'username';
    }
}
