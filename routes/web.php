<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Social Media Auth
Route::get('login/{provider}','Auth\LoginController@redirectToProvider')->name("login.social");
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get("/t/{tag}/","TagController@show")->name("tag");

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/', 'HomeController@index')->name('home');

//Route::resource('/posts', 'PostController');



Route::get("/{user}/{post}","PostController@show")->name("posts.show");



//Reactions
Route::get("/reactions","ReactionController@show");

Route::group(['middleware' => ['auth']], function () {
    //
    Route::get("/new","PostController@create")->name("posts.create");
    Route::post("/comments","CommentController@store")->name("comments.store");
    Route::post("/new","PostController@store")->name("posts.store");



    Route::post("/reactions/","ReactionController@react")->name("react");

    Route::get("/{user}/{post}/edit","PostController@edit")->name("posts.edit");
    Route::post("/{user}/{post}/edit","PostController@update")->name("posts.update");

    Route::get("/settings","UserController@edit")->name("users.edit");
    Route::post("/settings","UserController@update")->name("users.update");
});






Route::get("/{user}/","UserController@show")->name("users.profile");



