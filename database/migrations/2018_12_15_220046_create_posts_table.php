<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->string('title');
            $table->text('body_markdown');
            $table->text('body_html')->nullable();
            $table->string('slug')->index();
            $table->string('image')->nullable();
            $table->boolean("active")->default(true);
            $table->integer("hotness_score")->default(0);
            $table->integer("comments_cache")->default(0);
            $table->integer("hearts_cache")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
