<?php

use Faker\Generator as Faker;


$factory->define(App\Post::class, function (Faker $faker) {
    $faker->addProvider(new \DavidBadura\FakerMarkdownGenerator\FakerProvider($faker));
    return [
        //
        'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'body_markdown'=> $faker->markdown(),
        'user_id'=>$faker->numberBetween(1,1001),
        'slug'=> implode("-", $faker->words($nb = 3) ),
        'created_at'=> $faker->dateTimeThisYear(),
        'image'=>( rand ( 0 ,3 ) == 1)?"https://source.unsplash.com/random":null,
    ];
});

