<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        //
        'body_markdown'=> $faker->markdown(),
        'user_id'=>$faker->numberBetween(1,501),
        'post_id'=> $faker->numberBetween(0,10000),
        'created_at'=> $faker->dateTimeThisMonth(),
    ];
});
