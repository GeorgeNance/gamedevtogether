<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert(array(
            'email' => 'thatbuddhadude@gmail.com',
            'username'=>'buda',
            'name'=>'George Nance',
            'password'=> bcrypt('gamedev'),
            'profile_image'=>"https://pbs.twimg.com/profile_images/1076967741091803136/oNAKCZ_H_400x400.jpg",
            "location"=>"Tempe,AZ",
            "created_at" => "2018-12-16",
            'twitter_username'=>"dudebuda",
            'twitch_username'=>"budadude",
            'steam_username'=>"budadude"
    ));

        \App\User::first()->accounts()->create([
            'provider_id' => "313404490",
            'provider_name' => "twitter",
            'auth_data_dump'=>"[]"
        ]);
    }
}
