<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{

    private $tags = [
        "art",
        "audio",
        "javascript",
        "unity3d",
        "ios",
        "android",
        "godot",
        "gamemaker"
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach ($this->tags as $tag){
            DB::table('tags')->insert(array(
                'name'=>$tag,
            ));

        }

    }
}
