<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(TagsTableSeeder::class);
        factory(App\User::class, 1000)->create();
         factory(App\Post::class, 10000)->create();
        factory(App\Comment::class, 9000)->create();
    }
}
