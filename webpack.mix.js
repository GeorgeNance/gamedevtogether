
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const mix = require("laravel-mix");
const tailwindcss = require("tailwindcss");
//require("laravel-mix-purgecss");


mix.postCss('resources/css/stylesheet.css', 'public/css', [
    tailwindcss('tailwind.js'),
]).js('resources/js/app.js', 'public/js');
    //.purgeCss();